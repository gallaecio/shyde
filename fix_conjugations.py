#!/usr/bin/env python2
# -*- coding:utf-8 -*-

"""
Este guión atopa problemas en conxugacións verbais, e os soluciona de ser
posible.
"""


from bs4 import BeautifulSoup
from datadiff import diff
import json
import re
import requests

from pywikibot import Bot, Site
from pywikibot.page import Category, Link, Page
from pywikibot.pagegenerators import \
    PreloadingGenerator, \
    CategorizedPageGenerator


class ShydeNaGalipedia(Bot):

    def __init__(self, **kwargs):
        super(ShydeNaGalipedia, self).__init__(**kwargs)
        self.sitename = u"wiktionary"
        self.username = u"Shyde"

        # TODO: En vez de empregar o sitio web, descargar o «conshuga»
        # (é software libre) e chamalo dende Python. Crear un proxecto
        # independente para el dentro de Idiomatic.
        # Galician
        self.galicionario = self.site(u"gl")
        self.galician_url_template = u"http://gramatica.usc.es/pln/gl/tools/" \
            u"conjugador/conjugador.php?texto={verb}"
        self.galician_templates = {}
        term = ur"\{\{gl\|(\{\{\{\d+\}\}\}\w*)\}\}(?:<sup>1</sup>)?"
        self.galician_term = re.compile(u"(?u)" + term)
        self.galician_base_template_re = re.compile(ur"(?u)\{{\{{conx\.gl\n"
            ur"\|(?P<infinitivo>{term})"
            ur"\|(?P<xerundio>{term})"
            ur"\|\{{\{{\{{participio\|(?P<participio>{term}, {term}, {term}, {term})\}}\}}\}}\n"
            ur"\|(?P<indicativo_presente>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<indicativo_copreterito>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<indicativo_preterito>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<indicativo_antepreterito>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<indicativo_futuro>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<indicativo_pospreterito>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<subxuntivo_presente>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<subxuntivo_preterito>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<subxuntivo_futuro>{term}\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<imperativo_afirmativo>\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<imperativo_negativo>\|{term}\|{term}\|{term}\|{term}\|{term})\n"
            ur"\|(?P<infinitivo_persoal>{term}\|{term}\|{term}\|{term}\|{term}\|{term})".format(
                term=term))


    def site(self, language):
        return Site(language, self.sitename, self.username)


    def fetch_galician_expected_conjugation(self, verb):
        response = requests.get(self.galician_url_template.format(verb=verb))
        response.encoding = "utf-8"
        soup = BeautifulSoup(response.text)
        table = soup.find("table")
        if not table:
            print(u"Descoñécese a conxugación correcta de «{}». "
                u"Seguro que é un verbo correcto?".format(verb))
            raise KeyError()
        for font in table.find_all("font"):
            font.decompose()
        conjugation = {}
        for conjugation_name, index in (
                    ("indicativo presente", 0),
                    ("indicativo preterito", 1),
                    ("indicativo copreterito", 2),
                    ("indicativo antepreterito", 3),
                    ("indicativo futuro", 4),
                    ("indicativo pospreterito", 5),
                    ("subxuntivo presente", 6),
                    ("subxuntivo preterito", 7),
                    ("subxuntivo futuro", 8),
                    ("infinitivo persoal", 9),
                    ("imperativo afirmativo", 10),
                    ("imperativo negativo", 11),
                ):
            conjugation[conjugation_name] = \
                table.find_all("td")[index].get_text().strip().split()
        nominal_forms = table.find_all("td")[12].get_text().strip().split()
        conjugation["infinitivo"] = nominal_forms[0]
        conjugation["xerundio"] = nominal_forms[1]
        conjugation["participio"] = [
                nominal_forms[2][:-1] + u"a",
                nominal_forms[2][:-1] + u"as",
                nominal_forms[2][:-1] + u"o",
                nominal_forms[2][:-1] + u"os",
            ]
        return conjugation


    def fetch_galician_actual_conjugation(self, template_name, args):
        if template_name not in self.galician_templates:
            template = Page(Link(u"Template:" + template_name,
                                 source=self.galicionario))
            content = template.get()
            match = self.galician_base_template_re.search(content)
            if not match:
                print(u"O modelo de conxugación «{}» non ten a forma esperada."
                    .format(template_name))
                raise NotImplementedError()
            self.galician_templates[template_name] = {}
            for key in ("infinitivo", "xerundio"):
                self.galician_templates[template_name][key] = \
                    match.group(key)[5:-2]
            participios = [item[5:-2]
                           for item in match.group("participio").split(", ")]
            self.galician_templates[template_name]["participio"] = [
                participios[1], participios[3], participios[0], participios[2]]
            for key in (
                    u"indicativo presente",
                    u"indicativo preterito",
                    u"indicativo copreterito",
                    u"indicativo antepreterito",
                    u"indicativo futuro",
                    u"indicativo pospreterito",
                    u"subxuntivo presente",
                    u"subxuntivo preterito",
                    u"subxuntivo futuro",
                    u"infinitivo persoal",
                    u"imperativo afirmativo",
                    u"imperativo negativo",
                    ):
                self.galician_templates[template_name][key] = [
                    item for item in self.galician_term.sub(
                        ur"\1",
                        match.group(key.replace(u" ", u"_"))
                        ).split(u"|")
                    if item]
        string = json.dumps(self.galician_templates[template_name])
        for index, arg in enumerate(args):
            string = string.replace(u"{{{" + unicode(index+1) + u"}}}", arg)
        return json.loads(string)


    def fix_galician_conjugations(self):
        verbs = Category(Link(u"Categoría:Verbos en galego",
                              source=self.galicionario))
        generator = CategorizedPageGenerator(verbs, recurse=True,
                                               content=True)
        generator = PreloadingGenerator(generator)
        base_conjugation_template_re = re.compile(
            ur"\{\{ *(?P<template>[Cc]onx\.gl[^|]*?) *\|(?P<args>[^}]+)\}\}")
        end_conjugation_template_re = re.compile(
            ur"\{\{ *(?P<template>[Cc]onx\.gl[^|]*?) *\|(?P<args>[^}]+)\}\}")
        for page in generator:
        #for page in [Page(Link(u"abalanzar", source=self.galicionario)),]:
            content = page.get()
            verb = page.title()
            try:
                expected_conjugation = self.fetch_galician_expected_conjugation(verb)
                for match in end_conjugation_template_re.finditer(content):
                    actual_conjugation = self.fetch_galician_actual_conjugation(
                        match.group("template"), match.group("args").split(u"|"))
                    if cmp(expected_conjugation, actual_conjugation) != 0:
                        print(u"A conxugación para «{}» no Galicionario non se "
                            u"corresponde coa conxugación que se indica como "
                            u"correcta na seguinte ligazón:\n\n\t{}",format(
                                verb,
                                self.galician_url_template.format(verb=verb)))
                        print(diff(actual_conjugation, expected_conjugation,
                                fromfile=u"Conxugación no Galicionario",
                                tofile=u"Conxugación correcta"))
            except KeyError():
                pass


    def run(self):
        self.fix_galician_conjugations()


def main():
    bot = ShydeNaGalipedia()
    bot.run()



if __name__ == "__main__":
    main()