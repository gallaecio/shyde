#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This bot will parse the content of the specified pages, and normalize it so
that it uses the latest formatting agreements.
"""

"""
Parser tree example:

{
    "gl": {
        "pronuncia": "",
        "audio": ""
        "entries": [
            {
                "etimology": "",
                "partsOfSpeech": {
                    "adx": {
                        "var": None,
                        "definitions": [
                            {
                                "definition": "Que ten forma de [[baga]]."
                            },
                            {},
                            …
                        ]
                    },
                    "subst": {},
                    …
                }
            },
            {},
            …
        ],
    },
    "es": {},
    …,
    "categories": {
        "Categoría auxliar": None,
        "Música": None,
        …
    }
}
"""

import os, pickle, re
import PyICU

import pywikibot
from pywikibot import pagegenerators

import variables
variables.getLanguages() # For some reason, if you do not call this before you import fixes, it gets stuck.

from sortTranslations import generateSortedTranslationBlock


collator = PyICU.Collator.createInstance(PyICU.Locale('gl.UTF-8'))


def getLanguages():

  global languages
  try:
      return languages
  except:
      pass

  languages = []

  page = pywikibot.Page(pywikibot.getSite(), u"Modelo:nomelingua")
  pattern = re.compile(u"([^ ]*) *= +(.*)")
  lines = page.get().split('\n')

  for line in lines:
    matches = pattern.search(line)
    if matches:
      if matches.group(1) != u"nc": # Skip nc, to be added later.
        languages.append((matches.group(1), matches.group(2)))

  return languages


def getPartsOfSpeech():

    global partsOfSpeech
    try:
        return partsOfSpeech
    except:
        pass

    cachePath = "normalize.pof.cache"
    if os.path.exists(cachePath):
        return pickle.load(open(cachePath, "rb"))

    partsOfSpeech = []

    wikiCategory = pywikibot.page.Category(pywikibot.getSite(), "%s:%s" % (pywikibot.getSite().namespace(14), u'Wiktionary:Modelos de categoría gramatical'))

    for page in wikiCategory.articles():
        partOfSpeechCode = page.title()[8:-1]
        match = regex["partOfSpeechTemplate"].search(page.get())
        if match:
            partOfSpeechName = match.group("name")
        else:
            raise pywikibot.Error("Part of speech template {} has no name".format(partOfSpeechCode))
        partsOfSpeech.append((partOfSpeechCode, partOfSpeechName))

    pickle.dump(partsOfSpeech, open(cachePath, "wb"))

    return partsOfSpeech


def partOfSpeechFromDisplayName(displayName):
    for code, name in getPartsOfSpeech():
        if name == displayName:
            return code

commonLanguage = "gl"


regex = {
    # Current syntax.
    "language": re.compile('\{\{ *-(' + variables.getLanguages() + '|glref)- *\}\}'),
    "etim": re.compile(ur'\{\{ *etim *\| *(.*?) *(\| *[a-z]+ *)?\}\}$'),
    "pronuncia": re.compile(ur'\{\{ *pronuncia *\| *(.*?) *(\| *[a-z]+ *)?\}\}$'),
    "partOfSpeech": re.compile('\{\{ *-(' + u"|".join([partOfSpeech[0] for partOfSpeech in getPartsOfSpeech()]) + ')- *(\| *[a-z]+ *)?\}\}'),
    "category": re.compile(ur'\[\[ *Categoría *: *([^]|]+) *(\| *[^]]+ *)?\]\]'),
    "interwiki": re.compile(ur'\[\[ *([a-z]+) *: *([^]|]+) *(\| *[^]]+ *)?\]\]'),

    # Old syntax.
    "partOfSpeechOldHeader": re.compile(ur"=== *(" + u"|".join([partOfSpeech[1] for partOfSpeech in getPartsOfSpeech()]) + ") *==="),
    "definitionsOldHeader": re.compile(ur"==== *Definicións *===="),
    "etimologyOldHeader": re.compile(ur"==== *(" + u"|".join([u"Etimoloxía",]) + ") *===="),
    "exampleOldHeader": re.compile(ur"==== *(" + u"|".join([u"Exemplo", u"Exemplos"]) + ") *===="),
    "relacOldHeaders": re.compile(ur"==== *(" + u"|".join([u"Nome do froito", u"Nome do colectivo", u"Termos relacionados"]) + ") *===="),
    "scientificNameOldHeader": re.compile(ur"==== *(" + u"|".join([u"Nome científico",]) + ") *===="),
    "simpleLink": re.compile(ur"\[\[([^]]+?) *\]\]"),
    "maiLink": re.compile(ur"\{\{ *[Mm]ai *\| *([^}]+) *\}\}"),
    "wikipedia": re.compile(ur"(?:\* *Na *(?:[Gg]ali|[Ww]iki)pedia \[\[w:([^|]+)\||Ver na (?:[Gg]ali|[Ww]iki)pedia: \[http://gl\.wikipedia\.org/wiki/([^ ]+))"),
    "numberedSynonym": re.compile(ur"\*? *(\d+) *- *(.*)"),

    # Templates.
    "partOfSpeechTemplate": re.compile(ur"=== *(\[\[[^]]+?\]\])? *(?P<name>[^][=]+?) *==="),
    "flexion": re.compile(ur"\{\{[Vv]ar[Xx]?[Nn]\|"),
    "oldFlexion": re.compile(ur"\{\{[cC][Vv]ar[Xx]?[Nn]\|"),

    # Etimology constructions
    "etim:de": re.compile(ur"de ''\[\[([^]]+)\]\]''\."),
    "etim:de-e": re.compile(ur"de ''\[\[([^]]+)\]\]'' e ''\[\[([^]]+)\]\]''\."),

    # Definition constructions
    "sem": re.compile(ur"\{\{ *sem *\| *([^|}]+) *(\| *[a-z]+ *)?\}\}"),

    # Translation constructions
    "translation-numbers": re.compile(ur"\( *(\d+(, *\d+)*) *\)"),
    "translation-prefix": re.compile(ur"\{\{ *T *\| *([a-z]+) *\}\}"),
    "translation-simply-linked-term": re.compile(ur"\[\[([^]]+)\]\]"),
    "translation-unlinked-term": re.compile(ur"''([^']+)''"),

    # Color constructions
    "color": re.compile(u"bgcolor=\"([^\"]+)\""),
}


class PageParser(object):

    def __init__(self, pageTitle, pageContent):
        self.pageTitle = pageTitle
        self.pageContent = pageContent
        self.tree = {
            "categories": {},
            "interwiki": set(),
            "homo": set(),
        }
        self.currentLanguage = None
        self.currentEntryIndex = None
        self.currentPartOfSpeech = None
        self.inGlobalColorTable = False
        self.inGlobalConx = False
        self.inGlobalEtimology = False
        self.inGlobalExample = False
        self.inGlobalRelac = False
        self.inGlobalScientificName = False
        self.inGlobalSeeAlso = False
        self.inGlobalSynonyms = False
        self.inGlobalTranslations = False
        self.glref = False
        self.globalColors = []
        self.globalTranslations = set()


    def parseExampleLine(self, line):
        line = line[2:].strip()
        if line.endswith("."):
            line = line[:-1]
        return line[10:-2]


    def endParsing(self):
        self.resetToPartOfSpeech()


    def resetToPartOfSpeech(self):

        if self.inGlobalTranslations or self.inGlobalScientificName:
            definitions = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"]
            for definition in definitions:
                if "translations" not in definition:
                    definition["translations"] = {}
            for translation in self.globalTranslations:
                language, terms = self.extractTermsFromUnnumberedTranslationLine(translation)
                for definition in definitions:
                    if language not in definition["translations"]:
                        definition["translations"][language] = set()
                    for term in terms:
                        definition["translations"][language].add(term)
            self.globalTranslations = set()

        if self.inGlobalColorTable:
            if "colors" not in self.tree[self.currentLanguage]:
                self.tree[self.currentLanguage]["colors"] = []
            for color in self.globalColors:
                self.tree[self.currentLanguage]["colors"].append(color)
            self.globalColors = []

        self.inGlobalColorTable = False
        self.inGlobalConx = False
        self.inGlobalEtimology = False
        self.inGlobalExample = False
        self.inGlobalRelac = False
        self.inGlobalScientificName = False
        self.inGlobalSeeAlso = False
        self.inGlobalSynonyms = False
        self.inGlobalTranslations = False

    def extractSemFromDefinition(self, definition):
        sem = set()
        if definition.startswith(u"("):
            semString, definition = definition[1:].split(u")")
            for match in regex["sem"].finditer(semString):
                sem.add(match.group(1))
        if definition.startswith(u"''fig.'' "):
            definition = definition[9:]
            sem.add(u"Figurado")
        return definition.lstrip(), sem


    def ensureLanguage(self):
        if not self.currentLanguage:
            language = u"gl"
            self.tree[language] = {
                "pronuncia": "",
                "audio": "",
                "entries": [{
                    "etimology": "",
                    "partsOfSpeech": {},
                },],
            }
            self.currentLanguage = language
            self.currentEntryIndex = 0


    def parseLine(self, line):

        # Skip empty lines.
        if not line.strip():
            return

        line = line.rstrip()


        # Standard syntax

        # e.g. {{-gl-}}
        match = regex["language"].match(line)
        if match:
            language = match.group(1)
            if language == u"glref":
                self.glref = True
                language = u"gl"
            if language not in self.tree:
                self.tree[language] = {
                    "pronuncia": "",
                    "audio": "",
                    "entries": [{
                        "etimology": "",
                        "partsOfSpeech": {},
                    },],
                }

            self.resetToPartOfSpeech()
            self.currentLanguage = language
            self.currentEntryIndex = 0
            return

        # e.g. {{etim| De ''[[abelá]]''.}}
        match = regex["etim"].match(line)
        if match:
            etimology = match.group(1)
            entry = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]
            entry["etimology"] = self.fixUpEtimology(etimology, self.currentLanguage)
            return

        # e.g. {{pronuncia|/abalaŋˈθaɾ/|gl}}
        match = regex["pronuncia"].match(line)
        if match:
            print match.group(1)
            self.tree[self.currentLanguage]["pronuncia"] = match.group(1)
            return

        # e.g. {{-substf-}}
        match = regex["partOfSpeech"].match(line)
        if match:
            self.ensureLanguage()
            entry = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]
            partOfSpeech = match.group(1)
            self.currentPartOfSpeech = partOfSpeech
            if partOfSpeech not in entry["partsOfSpeech"]:
                entry["partsOfSpeech"][partOfSpeech] = {
                    "var": None,
                    "definitions": [],
                }
            return

        # e.g. '''{{PAGENAME}}''' {{VarN|abelaneira||s}}
        if line.startswith(u"'''{{PAGENAME}}'''"):
            if line.endswith("}"):
                partOfSpeech = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]
                partOfSpeech["var"] = line[18:].strip()
                return


        if line.startswith("#*"):
            # e.g. #*{{exemplo|Mollou a brouca antes entrar nas ondas do mar.}}
            if u"{{exemplo|" in line:
                definition = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"][self.currentDefinition]
                if "examples" not in definition:
                    definition["examples"] = []
                definition["examples"].append(self.parseExampleLine(line))
                return
            else:
                raise pywikibot.Error("Subordinate definition lines other than examples are not implemented yet")

        # e.g. # Que ten forma de [[baga]].
        if line.startswith("#"):
            definitions = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"]
            definition, sem = self.extractSemFromDefinition(line[1:].strip())
            definitions.append({
                "definition": self.fixUpDefinition(definition, self.currentLanguage),
                "sem": sem,
            })
            self.currentDefinition = len(definitions)-1
            return

        # e.g. {{-trad-}}
        if line == u"{{-trad-}}":
            self.resetToPartOfSpeech()
            self.inGlobalTranslations = True
            return

        if line.startswith(u"{{T|"):
            # e.g. {{T|de}} [[Fußsohle]] (3)
            if any(char.isdigit() for char in line):
                self.parseLineWithNumberedTranslations(line)
            # e.g. {{T|de}} {{t|de|nacken}}.
            else:
                self.globalTranslations.add(self.fixUpTranslationLine(line))
            return

        # e.g. {{-ver-}}
        if line == u"{{-ver-}}":
            self.resetToPartOfSpeech()
            self.inGlobalSeeAlso = True
            return

        # e.g.
        # 1 - [[embaixo]]
        # *8- atreverse, decidirse
        if self.inGlobalSynonyms:
            match = regex["numberedSynonym"].search(line)
            if match:
                definitionIndex = int(match.group(1))-1
                definition = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"][definitionIndex]
                if "synonyms" not in definition:
                    definition["synonyms"] = set()
                for part in match.group(2).split(","):
                    synonym = part.strip()
                    if synonym.startswith("[["):
                        synonym = synonym[2:-2] # e.g. * [[caluga]] → caluga
                    if synonym: # Skip cases such as [[]]
                        definition["synonyms"].add(synonym)
                return

        # e.g. *Na wikipedia [[w:abruñeiro|abruñeiro]]
        match = regex["wikipedia"].match(line)
        if match:
            self.glref = next(item for item in match.groups() if item is not None)
            return

        # e.g. * [[caluga]]
        if line.startswith("*"):

            if self.inGlobalSynonyms:
                definitions = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"]
                for definition in definitions:
                    if "synonyms" not in definition:
                        definition["synonyms"] = set()
                for part in line[1:].split(","):
                    synonym = part.strip()
                    if synonym.startswith("[["):
                        synonym = synonym[2:-2] # e.g. * [[caluga]] → caluga
                    if synonym.startswith("{{"):
                        synonym = synonym[5:-2] # e.g. * {{Gl|artello}} → artello
                    if synonym: # Skip cases such as [[]]
                        for definition in definitions:
                            definition["synonyms"].add(synonym)
                return

            elif self.inGlobalRelac:
                relacTerm = line[1:].strip()[2:-2] # e.g. * [[caluga]] → caluga
                definitions = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"]
                for definition in definitions:
                    if "relac" not in definition:
                        definition["relac"] = set()
                    definition["relac"].add(relacTerm)
                return

            elif self.inGlobalScientificName:
                name = line[1:].strip()[4:-4] # e.g. * ''[[Prunus spinosa]]'' → Prunus spinosa
                if name: # Skip entries such as * ''[[]]''
                    translation = u"{{{{T|nc}}}} {{{{t|nc|{name}}}}}.".format(name=name)
                    self.globalTranslations.add(translation)
                return

            elif self.inGlobalSeeAlso:

                if "seealso" not in self.tree:
                    self.tree["seealso"] = []
                self.tree["seealso"].append(line)
                return

            elif self.inGlobalEtimology:

                entry = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]

                # e.g. * Lat: cappula
                if u":" in line:
                    language, term = line[1:].split(u":")
                    language = language.lower().strip()
                    if language == "lat":
                        language = "la"
                    elif language == "prerrom":
                        language = "prerromano"
                    if term.endswith("."):
                        term = term[:-1]
                    term = term.strip().strip("'")

                    entry["etimology"] = u"do {{{{orixe|{sourceLanguage}|{targetLanguage}}}}} ''{{{{{sourceLanguage}|{term}}}}}''.".format(
                        sourceLanguage=language,
                        targetLanguage=self.currentLanguage,
                        term=term,
                    )

                else:
                    etimology = line[1:].strip()
                    etimology = etimology[0].lower() + etimology[1:]
                    entry["etimology"] = etimology

                return

            else:
                raise pywikibot.Error("Unordered list entry found in unhandled scope.")

        # e.g. [[Categoría:Categoría auxiliar]]
        match = regex["category"].match(line)
        if match:
            category = match.group(1)
            if category not in self.tree["categories"] and category != u"Categoría auxiliar":
                self.tree["categories"][category] = None
            return

        # e.g. [[ast:cacha]]
        match = regex["interwiki"].match(line)
        if match:
            self.tree["interwiki"].add(match.group(1))
            return

        # e.g. {{homo/actinio}}
        if line.startswith("{{homo/"):
            self.tree["homo"].add(line)
            return

        # e.g. {{-conx-}}
        if line == u"{{-conx-}}":
            self.resetToPartOfSpeech()
            self.inGlobalConx = True
            return

        # e.g. {{-conx-}}
        if line.startswith("{{conx."):
            partOfSpeech = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]
            partOfSpeech["conx"] = line
            return


        # Non-standard syntax

        # e.g. === Adxectivo ===
        match = regex["partOfSpeechOldHeader"].match(line)
        if match:
            entry = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]
            partOfSpeech = partOfSpeechFromDisplayName(match.group(1))
            self.currentPartOfSpeech = partOfSpeech
            if partOfSpeech not in entry["partsOfSpeech"]:
                entry["partsOfSpeech"][partOfSpeech] = {
                    "var": None,
                    "definitions": [],
                }
            return

        # e.g. {{VarN|meixela||s}}
        match = regex["flexion"].match(line)
        if match:
            partOfSpeech = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]
            partOfSpeech["var"] = line
            return

        # e.g. {{CVarN|planta||s}}
        match = regex["oldFlexion"].match(line)
        if match:
            partOfSpeech = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]
            partOfSpeech["var"] = line[:2] + line[3:]
            return

        # e.g. ==== Definicións ====
        match = regex["definitionsOldHeader"].match(line)
        if match:
            return

        # e.g. {{-sin-}}
        if line == u"{{-sin-}}":
            self.resetToPartOfSpeech()
            self.inGlobalSynonyms = True
            return

        # e.g. {{-relac-}}
        if line == u"{{-relac-}}":
            self.resetToPartOfSpeech()
            self.inGlobalRelac = True
            return

        # e.g. ==== Nome do froito ====
        match = regex["relacOldHeaders"].match(line)
        if match:
            self.resetToPartOfSpeech()
            self.inGlobalRelac = True
            return

        # e.g. ==== Nome científico ====
        match = regex["scientificNameOldHeader"].match(line)
        if match:
            self.resetToPartOfSpeech()
            self.inGlobalScientificName = True
            return

        # e.g. ==== Etimoloxía ====
        match = regex["etimologyOldHeader"].match(line)
        if match:
            self.resetToPartOfSpeech()
            self.inGlobalEtimology = True
            return

        # e.g. ==== Exemplo ====
        match = regex["exampleOldHeader"].match(line)
        if match:
            self.resetToPartOfSpeech()
            self.inGlobalExample = True
            return

        # e.g. <table border="0"  width="25%" cellspacing="5">
        if line.lstrip().startswith("<table") and self.inGlobalExample:
            self.resetToPartOfSpeech()
            self.inGlobalColorTable = True

        # e.g. <td width="33%" bgcolor="#FF0000">&nbsp;</td>
        if "bgcolor" in line and self.inGlobalColorTable:
            self.globalColors.append(regex["color"].search(line).group(1))

        # e.g. </table>
        if line.lstrip().startswith("</table>") and self.inGlobalColorTable:
            self.resetToPartOfSpeech()
            self.inGlobalExample = True


    def parse(self):
        for line in self.pageContent.splitlines():
            self.parseLine(line)
        self.endParsing()


    def addEndingDot(self, text):
        if not text.endswith("."):
            text += u"."
        return text


    def fixSentenceLinks(self, text, language):
        if text.startswith("[["):
            text = regex["simpleLink"].sub(ur"{{{{{}|\1}}}}".format(language.capitalize()), text, 1)
        text = regex["simpleLink"].sub(ur"{{{{{}|\1}}}}".format(language), text)
        text = regex["maiLink"].sub(ur"{{{{{}|\1}}}}".format(language.capitalize()), text)
        return text


    def fixUpDefinition(self, definition, language):
        if definition.endswith("}}"):
            definition = definition[:-2] # Remove remainigs from {{def|\n# <definition>}} template.
        definition = self.addEndingDot(definition)
        definition = self.fixSentenceLinks(definition, commonLanguage)
        return definition


    def fixUpEtimology(self, etimology, language):
        etimology = etimology[0].lower() + etimology[1:]
        etimology = self.addEndingDot(etimology)

        # e.g. de ''[[ameixeira]]'' e ''[[bravo|brava]]'' → de «{{gl|ameixeira}}» e «{{gl|bravo|brava}}»
        if regex["etim:de-e"].match(etimology):
            startTag = u"''"
            endTag = startTag
            if language == "gl":
                startTag = u"«"
                endTag = u"»"
            etimology = regex["etim:de-e"].sub(u"de " + startTag + u"{{" + language + ur"|\1}}" + endTag + u" e " + startTag + u"{{" + language + ur"|\2}}" + endTag + u".", etimology)

        # e.g. de ''[[abelá]]'' → de «{{gl|abelá}}»
        if regex["etim:de"].match(etimology):
            startTag = u"''"
            endTag = startTag
            if language == "gl":
                startTag = u"«"
                endTag = u"»"
            etimology = regex["etim:de"].sub(u"de " + startTag + u"{{" + language + ur"|\1}}" + endTag + u".", etimology)

        return etimology


    def fixUpTranslationLine(self, line):
        language = regex["translation-prefix"].search(line).group(1)
        line = regex["translation-simply-linked-term"].sub(ur"{{{{t|{}|\1}}}}".format(language), line)
        line = regex["translation-unlinked-term"].sub(ur"{{{{t|{}|\1}}}}".format(language), line)
        line = self.addEndingDot(line)
        return line


    def extractNumbersFromTranslation(self, translation):
        numbers = [int(number) for number in regex["translation-numbers"].search(translation).group(1).split(",")]
        translation = regex["translation-numbers"].sub("", translation).strip()
        return translation, numbers


    def formatTranslationTerm(self, term, language):
        term = regex["translation-simply-linked-term"].sub(ur"{{{{t|{}|\1}}}}".format(language), term)
        term = regex["translation-unlinked-term"].sub(ur"{{{{t|{}|\1}}}}".format(language), term)
        return term


    def extractTermsFromUnnumberedTranslationLine(self, line):
        if line.endswith("."):
            line = line[:-1]
        language = regex["translation-prefix"].search(line).group(1)
        terms = set()
        definitions = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"]
        for part in line[9:].split(";"):
            for subpart in part.split(","):
                terms.add(self.formatTranslationTerm(subpart, language))
        return language, terms

    def parseLineWithNumberedTranslations(self, line):
        language = regex["translation-prefix"].search(line).group(1)
        definitions = self.tree[self.currentLanguage]["entries"][self.currentEntryIndex]["partsOfSpeech"][self.currentPartOfSpeech]["definitions"]
        for part in line[9:].split(";"):
            term, numbers = self.extractNumbersFromTranslation(part)
            term = self.formatTranslationTerm(term, language)
            for number in numbers:
                index = number-1
                if "translations" not in definitions[index]:
                    definitions[index]["translations"] = {}
                if language not in definitions[index]["translations"]:
                    definitions[index]["translations"][language] = set()
                definitions[index]["translations"][language].add(term)

    def renderExample(self, example, language):
        example = self.addEndingDot(example)
        example = self.fixSentenceLinks(example, language)
        return example


    def renderDefinitionEntry(self, data, language):
        semString = u""
        if "sem" in data and data["sem"]:
            semString = u"({}) ".format(u", ".join([u"{{{{sem|{}|{}}}}}".format(sem, language) for sem in data["sem"]]))
        outputLines = [u"# {}{}".format(semString, data["definition"]),]
        if "examples" in data:
            for example in data["examples"]:
                outputLines.append(u"#* {{{{exemplo|{}}}}}".format(self.renderExample(example, language)))
        if "synonyms" in data and       data["synonyms"]:
            synonyms = sorted(data["synonyms"], cmp=collator.compare)
            outputLines.append(u"#* {{{{sin|{}.}}}}".format(u", ".join([u"{{{{{}|{}}}}}".format(language, synonym) for synonym in synonyms])))
        if "relac" in data:
            relac = sorted(data["relac"], cmp=collator.compare)
            outputLines.append(u"#* {{{{relac|{}.}}}}".format(u", ".join([u"{{{{{}|{}}}}}".format(language, relacTerm) for relacTerm in relac])))
        return u"\n".join(outputLines)


    def stripLanguageLinks(self, text, language):
        return re.sub(ur"\{\{(?:" + u"|".join([language, language.capitalize()]) + u")\|([^}]+)\}\}", ur"\1", text).capitalize()


    def renderPartOfSpeechEntry(self, partOfSpeech, data, language):
        outputLines = [u"{{{{-{}-|{}}}}}".format(partOfSpeech, language),]

        firstLine = u"'''{{PAGENAME}}'''"
        if u" " in self.pageTitle:
            firstLine = u"'''{}'''".format(" ".join([u"{{{{{}|{}}}}}".format(language, word) for word in self.pageTitle.split(u" ")]))
        if data["var"]:
            firstLine += u" " + data["var"]
        outputLines.append(firstLine)

        for definitionEntry in data["definitions"]:
            outputLines.append(self.renderDefinitionEntry(definitionEntry, language))

        thereAreTranslations = False
        for definitionEntry in data["definitions"]:
            if "translations" in definitionEntry:
                thereAreTranslations = True
        if thereAreTranslations:
            outputLines.append(u"{{-trad-}}")
            for definitionEntry in data["definitions"]:
                if "translations" in definitionEntry:
                    oneTemplate = u"{{{{1|{}}}}}".format(self.stripLanguageLinks(definitionEntry["definition"], language))
                    translationList = []
                    for translationLanguage in definitionEntry["translations"]:
                        translationList.append(u"{{{{T|{}}}}} {}.".format(translationLanguage, u", ".join(sorted(definitionEntry["translations"][translationLanguage], cmp=collator.compare))))
                    outputLines.append(generateSortedTranslationBlock(oneTemplate, translationList))

        if "conx" in data:
            outputLines.append(u"{{-conx-}}")
            outputLines.append(data["conx"])

        return u"\n".join(outputLines)



    def renderLanguageEntry(self, language, data):

        # Handling of unimplemented cases
        onlyOneEtimologyEntry = len(data["entries"]) < 2
        if not onlyOneEtimologyEntry:
            raise pywikibot.Error("Output of entries with more than one etimology is not implemented yet")

        languageTemplate = u"{{{{-{}-}}}}".format(language)
        if language == u"gl":
            if self.glref is True or self.glref == u"{{PAGENAME}}" or self.glref == self.pageTitle:
                languageTemplate = u"{{-glref-}}"
            elif self.glref:
                languageTemplate = u"{{{{-glref-|{}}}}}".format(self.glref)
        outputLines = [languageTemplate,]

        if "colors" in data:
            outputLines.append(u"{{{{cor|{}}}}}".format(u"|".join(data["colors"])))


        if onlyOneEtimologyEntry:
            outputLines.append(u"{{{{etim|{}|{}}}}}".format(data["entries"][0]["etimology"], language))
        outputLines.append(u"{{{{pronuncia|{}|{}}}}}".format(data["pronuncia"], language))
        outputLines.append(u"{{{{son|{}||{}}}}}".format(data["audio"], language))

        for entry in data["entries"]:
            for partOfSpeechCode, partOfSpeechName in sortedPartsOfSpeech:
                if partOfSpeechCode in entry["partsOfSpeech"]:
                    outputLines.append(self.renderPartOfSpeechEntry(partOfSpeechCode, entry["partsOfSpeech"][partOfSpeechCode], language))

        return u"\n".join(outputLines)



    def render(self):
        galego = self.tree.pop("gl", None)
        seeAlso = self.tree.pop("seealso", None)
        categories = self.tree.pop("categories")
        interwiki = self.tree.pop("interwiki")
        homo = self.tree.pop("homo")
        outputBlocks = []

        if homo:
            outputBlocks.append(u"\n".join(sorted(homo, cmp=collator.compare)))

        if galego:
            outputBlocks.append(self.renderLanguageEntry("gl", galego))
        for languageCode, languageName in sortedLanguages:
            if languageCode in self.tree:
                outputBlocks.append(self.renderLanguageEntry(languageCode, self.tree[languageCode]))

        if seeAlso:
            seeAlso.insert(0, u"{{-ver-}}")
            outputBlocks.append(u"\n".join(seeAlso))

        if categories:
            categoryStrings = []
            for category, sortKeyword in categories.iteritems():
                if sortKeyword:
                    categoryStrings.append(u"[[Categoría:{}|{}]]".format(category, sortKeyword))
                else:
                    categoryStrings.append(u"[[Categoría:{}]]".format(category))
            outputBlocks.append(u"\n".join(categoryStrings))

        if interwiki:
            outputBlocks.append(u"\n".join([u"[[{}:{}]]".format(code, self.pageTitle) for code in sorted(interwiki)]))

        return u"\n\n\n".join(outputBlocks).replace("  ", " ")

    def normalize(self):
        self.parse()
        return self.render()



def normalize(pageTitle, pageContent):
    parser = PageParser(pageTitle=pageTitle, pageContent=pageContent)
    normalizedContent = parser.normalize()
    return normalizedContent

def main(*args):

    # Read command-line parameters.

    local_args = pywikibot.handleArgs(*args)
    genFactory = pagegenerators.GeneratorFactory()

    for arg in local_args:
        if genFactory.handleArg(arg):
            continue
        else:
            raise pywikibot.Error('Unexpected argument: {}.'.format(arg))

    gen = genFactory.getCombinedGenerator()

    if not gen:
        # syntax error, show help text from the top of this file
        pywikibot.showHelp('normalize')
        return

    # Initialization

    print u":: Generating a sorted list of languages…"
    global sortedLanguages
    sortedLanguages = getLanguages()
    sortedLanguages.sort(key=lambda language: language[1], cmp=collator.compare)

    print u":: Generating a sorted list of parts of speech…"
    global sortedPartsOfSpeech
    sortedPartsOfSpeech = getPartsOfSpeech()
    sortedPartsOfSpeech.sort(key=lambda partOfSpeech: partOfSpeech[1], cmp=collator.compare)

    # Page loop

    print u":: Normalizing…"
    preloadingGen = pagegenerators.PreloadingGenerator(gen)

    acceptAll = False
    editSummary = "Normalización do formato"
    for page in preloadingGen:
        try:
            # Load the page's text from the wiki
            print u"Normalizing {}…".format(page.title())
            original_text = page.get()
            if not page.canBeEdited():
                pywikibot.output(u"You can't edit page %s"
                                    % page.title(asLink=True))
                continue
        except pywikibot.NoPage:
            pywikibot.output(u'Page %s not found' % page.title(asLink=True))
            continue
        new_text = original_text
        while True:
            new_text = normalize(pageTitle=page.title(), pageContent=new_text)
            if new_text == original_text:
                pywikibot.output(u'No changes were necessary in %s'
                                    % page.title(asLink=True))
                break
            # Show the title of the page we're working on.
            # Highlight the title in purple.
            pywikibot.output(u"\n\n>>> \03{lightpurple}%s\03{default} <<<"
                                % page.title())
            pywikibot.showDiff(original_text, new_text)
            if acceptAll:
                break
            choice = pywikibot.inputChoice(
                    u'Do you want to accept these changes?',
                    ['Yes', 'No', 'All', 'Quit'],
                    ['y', 'N', 'a', 'q'], 'N')
            if choice == 'q':
                return
            if choice == 'a':
                acceptAll = True
            if choice == 'y':
                page.put_async(new_text, editSummary)
            break
        if acceptAll and new_text != original_text:
            try:
                page.put(new_text, editSummary)
            except pywikibot.EditConflict:
                pywikibot.output(u'Skipping %s because of edit conflict'
                                    % (page.title(),))
            except pywikibot.SpamfilterError, e:
                pywikibot.output(
                    u'Cannot change %s because of blacklist entry %s'
                    % (page.title(), e.url))
            except pywikibot.PageNotSaved, error:
                pywikibot.output(u'Error putting page: %s'
                                    % (error.args,))
            except pywikibot.LockedPage:
                pywikibot.output(u'Skipping %s (locked page)'
                                    % (page.title(),))


if __name__ == "__main__":
    main()
