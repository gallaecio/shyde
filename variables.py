# -*- coding: utf-8 -*-

import re, sys
import PyICU

import pywikibot
from pywikibot import pagegenerators

import configuration

# LANGUAGES

langCodes = u''

def languages():
    global langCodes
    if not langCodes:
        langCodes = getLanguages()
    return langCodes

def getLanguages():

    languages = []

    page = pywikibot.Page(configuration.site, u"Modelo:nomelingua")
    pattern = re.compile('([^ ]*) *=')
    lines = page.get().split('\n')

    for line in lines:
        matches = pattern.search(line)
        if matches:
            languages.append(matches.group(1))

    return u'|'.join(languages)

# GRAMATICAL CATEGORIES

gramCats = ''

def categories():
    global gramCats
    if not gramCats:
        gramCats = getCategories()
    return gramCats

def getCategories():

    categories = []

    import catlib

    wikiCategory = catlib.Category(configuration.site, "%s:%s" % (configuration.site.namespace(14), u'Wiktionary:Modelos de categoría gramatical'))

    for page in wikiCategory.articles():
        categories.append(page.title()[8:-1])

    return u'|'.join(categories)

# LINE BREAK

n = u' *((< *br */?>)?\r?\n *|$)'