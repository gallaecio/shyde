# -*- coding: utf-8  -*-

import variables as var

fixes = {

    # Example.
    #'example': {
        #'msg': u'Substitución automática.',
        #'replacements': [
            #(r'(?i)([\r\n]) *<h6> *([^<]+?) *</h6> *([\r\n])',  r"\1====== \2 ======\3"),
        #]
    #},
    
    
    # MANTEMENTO

    # Corrixe o número de barras dos modelos etim, pronuncia e son.
    'barras': {
        'msg': u'{{etim|<lingua>}} → {{etim||<lingua>}} (tamén con {{pronuncia}} e {{son}}).',
        'replacements': [
            (u'\{\{ *(?P<modelo>etim|pronuncia) *\| *(' + var.getLanguages() + ') *\}\}', u'{{\g<modelo>||${LANGUAGE}}}'),
            (u'\{\{ *(?P<modelo>son) *\|( *\|)? *(' + var.getLanguages() + ') *\}\}', u'{{\g<modelo>|||${LANGUAGE}}}'),
        ]
    },

    # Corrixe os modelos sen pechar.
    'chaves': {
        'msg': u'Corrección dos modelos sen pechar.',
        'replacements': [
            # {{{asdf}}
            (u'^\{\{\{(?P<contido>[^{}]+)\}\}(?!\})', u'{{\g<contido>}}'),
            (u'(?<!\{)\{\{\{(?P<contido>[^{}]+)\}\}(?!\})', u'{{\g<contido>}}'),
            # {asdf}}
            (u'^\{(?P<contido>[^{}]+)\}\}(?!\})', u'{{\g<contido>}}'),
            (u'(?<!\{)\{(?P<contido>[^{}]+)\}\}(?!\})', u'{{\g<contido>}}'),
            # {{asdf}}}
            (u'(?<!{)\{\{(?P<contido>[^{}]+)\}\}\}$', u'{{\g<contido>}}'),
            (u'(?<!{)\{\{(?P<contido>[^{}]+)\}\}\}(?!\})', u'{{\g<contido>}}'),
            # {{asdf}
            (u'(?<!{)\{\{(?P<contido>[^{}]+)\}$', u'{{\g<contido>}}'),
            (u'(?<!{)\{\{(?P<contido>[^{}]+)\}(?!\})', u'{{\g<contido>}}'),
        ]
    },

}
