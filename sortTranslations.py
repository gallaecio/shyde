#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""
Script to order the lists of translations according to the name of the language.
"""

import re, sys
import PyICU

import pywikibot
from pywikibot import pagegenerators

import configuration, variables

collator = PyICU.Collator.createInstance(PyICU.Locale('gl.UTF-8'))

def getLanguages():

  languages = []

  page = pywikibot.Page(configuration.site, u"Modelo:nomelingua")
  pattern = re.compile(u"([^ ]*) *= +(.*)")
  lines = page.get().split('\n')

  for line in lines:
    matches = pattern.search(line)
    if matches:
      if matches.group(1) != u"nc": # Skip nc, to be added later.
        languages.append( (re.compile(u"\{\{ *T *\| *" + matches.group(1) + u" *\}\}"), matches.group(2)) )

  return languages

# Get a list of possible language codes.
global languages
languages = getLanguages()

# Sort them by name.
languages.sort(key=lambda language: language[1], cmp=collator.compare)

# Take “nc” to the first position on the list.
languages.insert(0, (re.compile('\{\{ *T *\| *nc *\}\}'), u'Nome científico')) # Only the language code is relevant at this point, though.


def generateTranslationBlock(oneTemplate, translationLines):

      length = len(translationLines)
      common = length // 3
      modulo = length % 3
      groups = []

      newText = oneTemplate + '\n'

      # First column.
      total = common
      if modulo != 0:
        total += 1
        modulo -= 1
      for x in range(total):
        newText += translationLines.pop(0) + '\n'

      newText += '{{2}}\n'

      # Second column.
      total = common
      if modulo != 0:
        total += 1 # No need to count module farther, since there are only three columns.
      for x in range(total):
        newText += translationLines.pop(0) + '\n'

      newText += '{{2}}\n'

      # Third column.
      for x in range(common):
        newText += translationLines.pop(0) + '\n'

      newText += '{{3}}'

      return newText



def generateSortedTranslationBlock(oneTemplate, translationLines):

      knownTranslations = []
      unknownTranslations = []

      for language in languages:
        for line in translationLines:
          lineMatches = language[0].search(line)
          if lineMatches:
            knownTranslations.append(line)
            # Do not break here. While it is not advisable, there might be several entries for the same language.

      for line in list(set(translationLines) - set(knownTranslations)):
        isTranslation = re.search(u"\{\{ *T *\|.*\}\}", line)
        if isTranslation:
          unknownTranslations.append(line)

      knownTranslations.extend(unknownTranslations) # Unrecogniced translation lines are appended at the end of the list.

      return generateTranslationBlock(oneTemplate, knownTranslations)



def sortTranslations(text):

  index = 0
  replacements = []

  # Parse complex lists (several columns).
  while True:
    # Detect a list of translations.
    matches = complexBlockPattern.search(text)
    if matches:
      matchedText = matches.group(0)
      un = matches.group('un')
      
      # Add a mark.
      mark = '@@' + str(index) + '@@'
      text = text.replace(matchedText, mark)
      index += 1
      
      lines = matchedText.split('\n')

      newText = generateSortedTranslationBlock(un, lines)

      # Define the replacement task.      
      replacements.append((mark, newText + u"\n"))

    else:
      break;
  
  # Parse simple lists.
  while True:
    
    # Detect a list of translations.
    matches = simpleBlockPattern.search(text)
    if matches:
      matchedText = matches.group(0)
      
      # Add a mark.
      mark = '@@' + str(index) + '@@'
      text = text.replace(matchedText, mark)
      index += 1
      
      lines = matchedText.split('\n')
      
      translationLines = []
      unknownTranslations = []

      for language in languages:
        for line in lines:
          lineMatches = language[0].search(line)
          if lineMatches:
            translationLines.append(line)
            # Do not break here. While it is not advisable, there might be several entries for the same language.
      for line in list(set(lines) - set(translationLines)):
        isTranslation = re.search('\{\{ *T *\|.*\}\}', line)
        if isTranslation:
          unknownTranslations.append(line)
            
      translationLines.extend(unknownTranslations) # Unrecogniced translation lines are appended at the end of the list.
      newText = ''
      for line in translationLines:
          newText += line + '\n'
            
      # Define the replacement task.
      replacements.append((mark, newText))
          
    else:
      break;
          
  # Replace back.
  for replacement in replacements:
    text = text.replace(replacement[0], replacement[1])
          
  return text
  
def writeEditCounter(counter):
  """ At the end of our work this writes the counter. """
  pywikibot.output(u'%d title%s saved.'
                   % (counter,
                     (lambda x: bool(x-1) and 's were' or ' was')
                     (counter)))

def textsAreDifferent(first, second):
    return first.replace('\r\n', '\n').replace('\r', '\n').strip() != second.replace('\r\n', '\n').replace('\r', '\n').strip()


# MAIN CODE #--------------------------------------------------------------------------------------#
def main(*args):                                 

  # Variables:  
  acceptall = False # Switch back to True once there is conficence that the latest changes have not messed up the script.
  editcounter = 0
  editSummary = u'Organización das listas de traducións.'

  global complexBlockPattern
  complexBlockPattern = re.compile('(?P<un>\{\{ *1.*?)(' + variables.n + ')+' +
                                  '((\{\{ *T *\| *[^}]* *\}\} *.*?)?' + variables.n + ')*' +
                                  '(\{\{ *2.*?' + variables.n +
                                  '((\{\{ *T *\| *[^}]* *\}\} *.*?)?' + variables.n + ')*)*' +
                                  '\{\{ *3.*?' + variables.n)

  global simpleBlockPattern
  simpleBlockPattern = re.compile( ' *\{\{ *T *\| *[^}]* *\}\} *.*?' + variables.n +
                                  '((\{\{ *T *\| *[^}]* *\}\} *.*?)?' + variables.n + ')+' +
                                  '\{\{ *T *\| *[^}]* *\}\} *.*?' + variables.n)
                                  
  genFactory = pagegenerators.GeneratorFactory()

  # Get the list of pages to work on.
  for arg in pywikibot.handleArgs(*args):
    genFactory.handleArg(arg)
  gen = genFactory.getCombinedGenerator()
  if not gen:
    pywikibot.output(u'You must specify what pages to work on. Try with “-transcludes:T” as parameter.')
    return
  preloadingGen = pagegenerators.PreloadingGenerator(gen, step=125)
  generator = preloadingGen

  # For each page:
  for page in generator:
    if page.namespace() != 0:
      pywikibot.output(u'Skipping %s because of its namespace'
                      % (page.title(),))
      continue
    try:
        # Load the page's text from the wiki
        original_text = page.get(get_redirect=True)
        if not page.canBeEdited():
            pywikibot.output(u"You can't edit page %s"
                              % page.title(asLink=True))
            continue
    except pywikibot.NoPage:
        pywikibot.output(u'Page %s not found' % page.title(asLink=True))
        continue
    new_text = sortTranslations(original_text)
    thereAreChanges = textsAreDifferent(new_text, original_text)
    while True:
      if not thereAreChanges:
          pywikibot.output(u'No changes were necessary in %s' % page.title(asLink=True))
          break

      # Show the title of the page we're working on.
      # Highlight the title in purple.
      pywikibot.output(u"\n\n>>> \03{lightpurple}%s\03{default} <<<"
                        % page.title())
      pywikibot.showDiff(original_text, new_text)
      if acceptall:
          break
      choice = pywikibot.inputChoice(
              u'Do you want to accept these changes?',
              ['Yes', 'No', 'All',
                'Quit'],
              ['y', 'N', 'a', 'q'], 'N')
      if choice == 'q':
          writeEditCounter(editcounter)
          sys.exit()
      if choice == 'a':
          acceptall = True
      if choice == 'y':
        # Primary behaviour: working on wiki
        page.text = new_text
        page.save(comment=editSummary, async=True)
        editcounter += 1
      # choice must be 'N'
      break
    if acceptall and thereAreChanges:
      try:
        page.text = new_text
        page.save(comment=editSummary)
        editcounter += 1 #increment only on success
      except pywikibot.EditConflict:
        pywikibot.output(u'Skipping %s because of edit conflict' % (page.title(),))
      except pywikibot.SpamfilterError, e:
        pywikibot.output(u'Cannot change %s because of blacklist entry %s' % (page.title(), e.url))
      except pywikibot.PageNotSaved, error:
        pywikibot.output(u'Error putting page: %s' % (error.args,))
      except pywikibot.LockedPage:
        pywikibot.output(u'Skipping %s (locked page)' % (page.title(),))

  #Finally:
  writeEditCounter(editcounter)

if __name__ == "__main__":
  main()
