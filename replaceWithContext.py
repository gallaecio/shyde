# -*- coding:utf-8 -*-

import os, re, sys

import pywikibot
from pywikibot import pagegenerators

import configuration
import variables
variables.getLanguages() # For some reason, if you do not call this before you import fixes, it gets stuck.
import fixes

class ReplaceWithContextRobot:
    """
    A bot that can do text replacements in the Galician Wiktionary with some context variables.
    """
    def __init__(self, generator, replacements, editSummary=''):
        """
        Arguments:
            * generator    - A generator that yields Page objects.
            * replacements - A list of 2-tuples of original text (as a
                             compiled regular expression) and replacement
                             text (as a string).
        """
        self.generator = generator
        self.replacements = replacements
        self.editSummary = editSummary
        self.editcounter = 0
        self.context = {}
        self.regex = {
            'LANGUAGE': re.compile('\{\{ *-(' + variables.getLanguages() + '|glref)- *\}\}' + variables.n),
        }
        self.acceptall = False

    def doReplacements(self, original_text):
        """
        Returns the text which is generated by applying all replacements to
        the given text.
        """
        new_text = original_text
        for old, new in self.replacements:
            new_text = self.replaceWithContext(new_text, old, new)
        return new_text

    def replaceWithContext(self, text, old, new):
        """
        Return text with 'old' replaced by 'new', in a line-by-line basis.

        It allows using context variables. The following variables are available, and will be parsed before compiling
        the regular expressions:
        - ${LANGUAGE}. Language code for the current line.
        - ${PAGENAME}. Title of the page being edited.

        Parameters:
            text            - a unicode string
            old             - a compiled or uncompiled regular expression
            new             - a unicode string (which can contain regular
                            expression references), or a function which takes
                            a match object as parameter. See parameter repl of
                            re.sub().
        """
        newText = ''
        for line in text.splitlines(1):

            # Update the context.
            match = self.regex['LANGUAGE'].search(line)
            if match:
                if match.group(1) == 'glref':
                    self.context['LANGUAGE'] = 'gl'
                else:
                    self.context['LANGUAGE'] = match.group(1)

            # Adapt old and new to the current context.
            contextOld = old
            contextNew = new
            for key, value in self.context.items():
                contextOld = contextOld.replace('${'+key+'}', value)
                contextNew = contextNew.replace('${'+key+'}', value)
            contextOld = re.compile(contextOld)

            # Replace.
            index = 0
            while True:
                match = contextOld.search(line, index)
                if not match:
                    break
                contextNew = contextNew.replace('\\n', '\n')
                replacement = contextNew
                groupR = re.compile(r'\\(?P<number>\d+)|\\g<(?P<name>.+?)>')
                while True:
                    groupMatch = groupR.search(replacement)
                    if not groupMatch:
                        break
                    groupID = groupMatch.group('name') or \
                            int(groupMatch.group('number'))
                    try:
                        replacement = replacement[:groupMatch.start()] + \
                                    match.group(groupID) + \
                                    replacement[groupMatch.end():]
                    except IndexError:
                        print '\nInvalid group reference:', groupID
                        print 'Groups found:\n', match.groups()
                        raise IndexError

                line = line[:match.start()] + replacement + line[match.end():]
                index = match.start() + len(replacement)

            newText += line

        return newText

    def writeEditCounter(self):
        """ At the end of our work this writes the counter. """
        pywikibot.output(u'%d page%s changed.' % (self.editcounter,
                    (lambda x: bool(x-1) and 's were' or ' was')
                    (self.editcounter)))

    def run(self):
        """
        Starts the robot.
        """
        # Run the generator which will yield Pages which might need to be
        # changed.
        for page in self.generator:
            self.context['PAGENAME'] = page.title()
            try:
                # Load the page's text from the wiki
                original_text = page.get(get_redirect=True)
                if not page.canBeEdited():
                    pywikibot.output(u"You can't edit page %s"
                                     % page.title(asLink=True))
                    continue
            except pywikibot.NoPage:
                pywikibot.output(u'Page %s not found' % page.title(asLink=True))
                continue
            new_text = original_text
            while True:
                new_text = self.doReplacements(new_text)
                if new_text == original_text:
                    pywikibot.output(u'No changes were necessary in %s'
                                     % page.title(asLink=True))
                    break
                # Show the title of the page we're working on.
                # Highlight the title in purple.
                pywikibot.output(u"\n\n>>> \03{lightpurple}%s\03{default} <<<"
                                 % page.title())
                pywikibot.showDiff(original_text, new_text)
                if self.acceptall:
                    break
                choice = pywikibot.inputChoice(
                        u'Do you want to accept these changes?',
                        ['Yes', 'No', 'All', 'Quit'],
                        ['y', 'N', 'a', 'q'], 'N')
                if choice == 'q':
                    self.writeEditCounter()
                    return
                if choice == 'a':
                    self.acceptall = True
                if choice == 'y':
                    page.put_async(new_text, self.editSummary)
                    self.editcounter += 1
                break
            if self.acceptall and new_text != original_text:
                try:
                    page.put(new_text, self.editSummary)
                    self.editcounter += 1 #increment only on success
                except pywikibot.EditConflict:
                    pywikibot.output(u'Skipping %s because of edit conflict'
                                        % (page.title(),))
                except pywikibot.SpamfilterError, e:
                    pywikibot.output(
                        u'Cannot change %s because of blacklist entry %s'
                        % (page.title(), e.url))
                except pywikibot.PageNotSaved, error:
                    pywikibot.output(u'Error putting page: %s'
                                        % (error.args,))
                except pywikibot.LockedPage:
                    pywikibot.output(u'Skipping %s (locked page)'
                                        % (page.title(),))

        #Finally:
        self.writeEditCounter()

fix = None
generator = None
generatorsFactory = pagegenerators.GeneratorFactory()

for arg in sys.argv[1:]:
    if arg.startswith('-fix:'):
        fix = arg[5:]
    else:
        if not generatorsFactory.handleArg(arg):
            pywikibot.output('The argument “{}” is an invalid parameter.'.format(arg))
            sys.exit()

fixname = fix

try:
    fix = fixes.fixes[fix]
except KeyError:
    pywikibot.output(u'Available predefined fixes are: {}.'.format(fixes.fixes.keys()))
    sys.exit()

if "msg" in fix:
    editSummary = fix['msg']

try:
    replacements = fix['replacements']
except KeyError:
    pywikibot.output(u'No replacements given in fix, don’t joke with me!')
    sys.exit()

generator = generatorsFactory.getCombinedGenerator()
if not generator:
    pywikibot.showHelp('replace')
    sys.exit()

preloadingGen = pagegenerators.PreloadingGenerator(generator, pageNumber=configuration.maxQuerySize)

bot = ReplaceWithContextRobot(preloadingGen, replacements, editSummary)
bot.run()
