#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-

"""Module of maintenance tasks to perform on Wikidata."""


import json
from os import path
from urllib import unquote_plus
from urlparse import urlparse, parse_qsl

from termcolor import colored
import pytz

import pywikibot
from pywikibot.bot import input_choice

from versiontracker import iter_version_info


version_property_code = "P348"
version_type_property_code = "P548"
stable_version_value = "Q12355314"
release_date_property_code = "P577"
reference_url_property_code = "P854"


def code_color(text):
    return colored(text, "yellow")


def item_color(text):
    return colored(text, "magenta")


def subitem_color(text):
    return colored(text, "cyan")


def new_color(text):
    return colored(text, "green")


def old_color(text):
    return colored(text, "red")


# Source: http://stackoverflow.com/a/9468284/939364
# (with modifications to ignore scheme and fragment)
class Url(object):
    """A url object that can be compared with other url objects
    without regard to the vagaries of encoding, escaping, and ordering
    of parameters in query strings."""

    def __init__(self, url):
        self.parts = urlparse(unicode(url))

    def __eq__(self, other):
        return (self.parts.netloc == other.parts.netloc and
                unquote_plus(self.parts.path) ==
                unquote_plus(other.parts.path) and
                self.parts.params == other.parts.params and
                frozenset(parse_qsl(self.parts.query)) ==
                frozenset(parse_qsl(other.parts.query)) and
                self.parts.fragment == other.parts.fragment)

    def __hash__(self):
        return hash([
            self.parts.netloc,
            unquote_plus(self.parts.path),
            self.parts.params,
            frozenset(parse_qsl(self.parts.query)),
            self.parts.fragment])

    def __contains__(self, item):
        return (self.parts.netloc == item.parts.netloc and
                unquote_plus(self.parts.path).startswith(
                    u"{}/".format(
                        unquote_plus(item.parts.path).rstrip(u"/"))) and
                self.parts.params == item.parts.params and
                frozenset(parse_qsl(self.parts.query)) ==
                    frozenset(parse_qsl(item.parts.query)) and
                self.parts.fragment == item.parts.fragment)


def load_software_mapping():
    file_path = path.join(path.dirname(__file__), u"software.json")
    with open(file_path, "r") as fp:
        return json.load(fp)


class SoftwareVersionUpdater(object):

    def __init__(self):
        self.detector_classes = {}
        self.apply_entity_changes = False
        self.skip_entity_changes = False

    def test(self):
        wikidata = pywikibot.Site("wikidata", "wikidata", interface="DataSite")
        software_mapping = load_software_mapping()
        software_pieces = software_mapping.keys()
        for item_data in iter_version_info(software_pieces):
            if isinstance(software_mapping[item_data["id"]], basestring):
                wikidata_codes = [software_mapping[item_data["id"]]]
            else:
                wikidata_codes = software_mapping[item_data["id"]].keys()
            for wikidata_code in wikidata_codes:
                item = pywikibot.ItemPage(wikidata, wikidata_code)
                item.get()

                # Version.
                claims = {}
                if version_property_code in item.claims:
                    claims = {
                        claim.getTarget(): claim
                        for claim in item.claims[version_property_code] }
                version = item_data["version"]
                if version not in claims:
                    return False

                # Version rank.
                for claim_version, claim in claims.iteritems():
                    actual_rank = claim.getRank()
                    expected_ranks = [u"deprecated", u"normal"]
                    if claim_version == version:
                        expected_ranks = [u"preferred",]
                    if actual_rank not in expected_ranks:
                        return False

                version_claim = claims[version]

                # Release date.
                if item_data["release_date"] and \
                        release_date_property_code not in version_claim.qualifiers:
                    return False

                # Reference.
                if item_data["reference_url"] and len(version_claim.sources) == 0:
                    return False

        return True

    def userApproves(self, question):
        approved = False
        decided = (self.always or self.apply_entity_changes or
                   self.skip_entity_changes)
        if not decided:
            answer = input_choice(question,
                                  [(u"Yes", u"y"), (u"No", u"n"),
                                   (u"Yes to all entity changes", u"e"),
                                   (u"No to all entity changes", u"s"),
                                   (u"Always", u"a")],
                                  default=u"n")
            if answer in [u"y", u"a", u"e"]:
                approved = True
                if answer == u"a":
                    self.always = True
                elif answer == u"e":
                    self.apply_entity_changes = True
            elif answer =='s':
                self.skip_entity_changes = True
        else:
            if self.always or self.apply_entity_changes:
                approved = True
                if self.always:
                    reason = u"always"
                else:
                    reason = u"entity"
                print(question + " ({})".format(reason))
            else:
                approved = False
        return approved

    def run(self):
        try:
            print(u":: Updating software version data…")
            wikidata = pywikibot.Site("wikidata", "wikidata", interface="DataSite")

            stable_version = pywikibot.ItemPage(wikidata, stable_version_value)

            software_mapping = load_software_mapping()
            software_pieces = software_mapping.keys()
            self.always = False
            for item_data in iter_version_info(software_pieces):
                if isinstance(software_mapping[item_data["id"]], basestring):
                    wikidata_codes = [software_mapping[item_data["id"]]]
                else:
                    wikidata_codes = software_mapping[item_data["id"]]
                for wikidata_code in wikidata_codes:
                    item = pywikibot.ItemPage(wikidata, wikidata_code)
                    item.get()
                    try:
                        item_name = item.labels["en"]
                    except KeyError:
                        item_name = u"<item without name in English>"

                    # Version.
                    claims = {}
                    if version_property_code in item.claims:
                        claims = {
                            claim.getTarget(): claim
                            for claim in item.claims[version_property_code] }
                    version = item_data["version"]
                    if version not in claims:
                        if self.userApproves(u"Do you want to add version ‘{}’ to ‘{}’ ({})?".format(
                                new_color(version), item_color(item_name), code_color(item.title()))):
                            version_property = pywikibot.PropertyPage(
                                wikidata, version_property_code)
                            claim = version_property.newClaim()
                            claim.setTarget(version)
                            item.addClaim(claim)
                            claims[version] = claim
                        else:
                            continue

                    # Version rank.
                    for claim_version, claim in claims.iteritems():

                        # Skip non-stable versions.
                        skip = False
                        for qualifier in claim.qualifiers.get(version_type_property_code, []):
                            if not qualifier.target_equals(stable_version):
                                skip = True
                                break
                        if skip:
                            continue

                        actual_rank = claim.getRank()
                        expected_ranks = [u"normal", u"deprecated"]
                        if claim_version == version:
                            expected_ranks = [u"preferred",]
                        if actual_rank not in expected_ranks:
                            if self.userApproves(
                                    u"Do you want to change the rank of version {} of ‘{}’ ({}) from "
                                    u"‘{}’ to ‘{}’?".format(
                                        subitem_color(claim_version), item_color(item_name),
                                        code_color(item.title()), old_color(actual_rank),
                                        new_color(expected_ranks[0]))):
                                claim.changeRank(expected_ranks[0])
                    version_claim = claims[version]

                    # Release date.
                    if item_data["release_date"] and \
                            release_date_property_code not in version_claim.qualifiers:
                        date = item_data["release_date"]
                        if date.tzinfo is not None and date.tzinfo.utcoffset(date) is not None:
                            date = date.astimezone(pytz.utc)
                        date = date.replace(hour=0, minute=0, second=0, microsecond=0)
                        if self.userApproves(
                                u"Do you want to add release date ‘{}’ to version ‘{}’ of ‘{}’ ({})?"
                                .format(
                                    new_color(date.strftime(u"%Y-%m-%d")), subitem_color(version),
                                    item_color(item_name), code_color(item.title()))):
                            iso_date = date.isoformat()
                            if iso_date.endswith("+00:00"):
                                iso_date = iso_date[:-6]
                            iso_date += "Z"
                            release_date_wbtime = pywikibot.WbTime.fromTimestr(
                                iso_date, precision=11, site=wikidata)
                            release_date_property = pywikibot.PropertyPage(
                                wikidata, release_date_property_code)
                            claim = release_date_property.newClaim()
                            claim.setTarget(release_date_wbtime)
                            version_claim.addQualifier(claim)

                    # Version type.
                    if version_type_property_code not in version_claim.qualifiers:
                        if self.userApproves(
                                u"Do you want to mark version ‘{}’ of ‘{}’ ({}) as ‘{}’ ({})?"
                                .format(
                                    subitem_color(version), item_color(item_name),
                                    code_color(item.title()), new_color(u"stable version"),
                                    code_color(stable_version_value))):
                            version_type_property = pywikibot.PropertyPage(
                                wikidata, version_type_property_code)
                            claim = version_type_property.newClaim()
                            claim.setTarget(stable_version)
                            version_claim.addQualifier(claim)

                    # Reference.
                    if item_data["reference_url"]:
                        url = item_data["reference_url"].replace(u" ", u"%20")
                        url_object = Url(url)
                        reference_exists = False
                        for claim_set in version_claim.sources:
                            if len(claim_set) > 1 or len(claim_set[claim_set.keys()[0]]) > 1:
                                # This script is not currently able to deal with claim sets other than
                                # those with a single claim that is a publication date.
                                reference_exists = True
                                break
                            claim = claim_set[claim_set.keys()[0]][0]
                            if claim.id != reference_url_property_code:
                                continue
                            claim_url_object = Url(claim.target)
                            if url_object == claim_url_object or url_object in claim_url_object:
                                reference_exists = True
                                break
                            elif claim_url_object in url_object:
                                if self.userApproves(u"Do you want to replace reference URL "
                                        u"‘{}’ with the more specific reference URL ‘{}’ in "
                                        u"version ‘{}’ of ‘{}’ ({})?".format(
                                            old_color(claim.target), new_color(url),
                                            subitem_color(version), item_color(item_name),
                                            code_color(item.title()))):
                                    try:
                                        claim.changeTarget(url)
                                    except NotImplementedError:
                                        reference_url_property = pywikibot.PropertyPage(
                                            wikidata, reference_url_property_code)
                                        new_claim = reference_url_property.newClaim()
                                        new_claim.setTarget(url)
                                        version_claim.addSource(new_claim)
                                        version_claim.removeSource(claim)
                                reference_exists = True
                                break
                        if not reference_exists:
                            if self.userApproves(
                                    u"Do you want to add reference URL ‘{}’ to version ‘{}’ of ‘{}’ "
                                    u"({})?".format(
                                        new_color(url), subitem_color(version), item_color(item_name),
                                        code_color(item.title()))):
                                reference_url_property = pywikibot.PropertyPage(
                                    wikidata, reference_url_property_code)
                                claim = reference_url_property.newClaim()
                                claim.setTarget(url)
                                version_claim.addSource(claim)
                    self.apply_entity_changes = False
                    self.skip_entity_changes = False
        except pywikibot.bot_choice.QuitKeyboardInterrupt:
            pass


def update_software_versions():
    task = SoftwareVersionUpdater()
    task.run()

def wikidata_is_up_to_date():
    task = SoftwareVersionUpdater()
    return task.test()
